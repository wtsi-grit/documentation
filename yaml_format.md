# Curation request YAML format

**Errors are very common when submitting YAML for the first time. If submitting for the first time, you should consult the "Common errors" section below, and run the YAML through a YAML validator such as http://www.yamllint.com/**

Each curation request email sent to grit-jira@sanger.ac.uk should be accompanied by a single YAML file specifying details of the assembly files and the process used to create them. Having this information specified in a standard, machine-readable manner allows us to automate some aspects of ticket processing.

You can attach the YAML file to the curation request email. Alternatively, for assemblies created within the Sanger Institute, the full path to the YAML file location on disk can be specified in the body of the email in a line starting "YAML: " and continuing with the file location.

If you have multiple curation requests, each one should have a separate email with a single associated YAML file. Do not attach multiple YAML files to a single curation request email or specify multiple YAML files in the body of the email.

The YAML filename should have the suffix ".yaml" or ".yml".

In the description below, mandatory fields are preceded by the word [MANDATORY]. **This word shouldn't actually appear in the YAML!** Fields preceded by [MANDATORY_FOR_NON_SANGER] are mandatory for assemblies produced outside the Sanger where the relevant data is available. The "assembled_by_group" field is required for any assembly other than Darwin/ASG assemblies produced within the Sanger ToL programme.

The specimen name should, if possible, conform to ToLID format, eg qxSacCarc1. If that is not possible, then it's highly preferable for the sample name to start with the same first letter that would be used for the ToLID (eg "i" for insects). Even if the specimen name does not correspond to a ToLID, the specimen name should use only alphanumeric characters, avoiding other special characters (including ".", "_", and "-"). This is because this specimen name is used as the basis for a MySQL database name, and should therefore avoid characters that are not permitted in MySQL database names.

There should always be a location for an assembly, so at least one of the following fields should be filled in: `primary, maternal, paternal, hap1`.

FASTA files should be gzipped, and the full path to the file should be provided in each case, eg `s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/assembly_MT_rockefeller/rGopFla2.MT.20191216.fasta.gz`. Short NextCloud links like `https://ergapilot.bsc.es/index.php/f/53084` unfortunately don't work- the full version of the link should be provided, including the file name.

Where a field is not applicable, that field should simply be omitted. In particular, where a particular assembly type doesn't exist (usually haplotigs or mito), the relevant field should simply be omitted. Do *not* provide the key for the field and leave it empty, and do *not* supply a value such as "N/A".

The `data_location` field pertains to all the assembly and data fields. The possible values are:

* `Sanger RW`: For files on Sanger drives in directories that GRIT have write access to, and to which we are expected to write temporary files. This is assumed to be the default, and will be used for Darwin projects.

* `Sanger RO`: For files on Sanger drives in directories to which GRIT have read-only access. This is likely to be the case for faculty projects.

* `S3`: For files on GenomeArk S3

* `FTP`: For files accessible via FTP (including ERGA NextCloud)

* `ERGA`: For ERGA NextCloud

The `pipeline` field, whilst not mandatory, is strongly encouraged for any assembly that Sanger is planning to submit.

Read directory locations are not required for Sanger DToL assemblies, where these are found at a predictable location in the directory structure, but they are required for all other assemblies. Preferred file formats differ for the different file types as described below. Note that these formats are preferences, not requirements.

## Common errors

* The word [MANDATORY] is used to indicate mandatory fields and should not be used in the YAML itself.

* Multi-line strings, as typically found in the "stats" and "notes" sections, must be indented in a uniform fashion (typically two spaces) and the field name must be followed by a pipe ("|") symbol.

* The "projects" field is a YAML list rather than a standard field, requiring each entry to be on its own line and prefixed with a hyphen even if there is a single project. This is because some assemblies belong to multiple projects. See the examples below.

* The full path to each gzipped FASTA file should be provided, whether a local file or a web-accessible resource, eg `s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/assembly_MT_rockefeller/rGopFla2.MT.20191216.fasta.gz`, rather than merely a directory name. Raw read locations such as `pacbio_read_dir` are, however, directories.

## Full details of fields

```
---
[MANDATORY] species: species name
[MANDATORY] specimen: ToLID for specimen. A single TOLID should be provided even if data from multiple specimens was combined to create the assembly.
[MANDATORY] projects: YAML list of relevant projects: VGP orders, VGP+, Darwin, Faculty, ASG, ERGA, Other
release_to: Where to release the sequence to: ENA, S3, FTP, ERGA, Internal. ENA means submit to ENA, S3 means upload to GenomeArk S3, FTP means upload to FTP location (see field below), ERGA means upload to ERGA NextCloud, Internal means provide to Sanger. By default this is assumed to be ENA for DToL projects and S3 for VGP orders. This field is strongly recommended for anything other than DToL assemblies
domain: eukaryota, bacteria, or archaea; this is "eukaryota" by default, and so can be omitted in most cases
cobiont_status: host or cobiont; this is "host" by default, and so can be omitted in most cases
host_specimen: For cobionts only: TOLID for host specimen
release_ftp_url: If "release_to" is FTP, then provide an FTP destination URL here
data_location: Should be one of: Sanger RW, Sanger RO, FTP, ERGA, or S3. By default this is assumed to be Sanger RW
primary: Gzipped FASTA file for primary sequence; this can be a file on Sanger drives or an S3 location. This must be the full path to the file, including the file name.
haplotigs: Gzipped FASTA file for haplotigs. This must be the full path to the file, including the file name.
mito: Gzipped FASTA file for MT assembly. This must be the full path to the file, including the file name.
plastid: Gzipped FASTA file for plastid assembly. This must be the full path to the file, including the file name.
mito_reference: Reference used to aid MT assembly. This must be the full path to the file, including the file name.
chloro_reference: Reference used to aid plastid assembly. This must be the full path to the file, including the file name.
maternal: Gzipped FASTA file for maternal haplotype assembly. This must be the full path to the file, including the file name.
paternal: Gzipped FASTA file for paternal haplotype assembly. This must be the full path to the file, including the file name.
hap1: Gzipped FASTA file for first haplotype-resolved assembly (eg from Hifiasm-HiC) which is not known to be maternal or paternal. This must be the full path to the file, including the file name.
hap2: Gzipped FASTA file for second haplotype-resolved assembly (eg from Hifiasm-HiC) which is not known to be maternal or paternal. This must be the full path to the file, including the file name.
haplotype_to_curate: For haplotype-resolved assemblies (eg from Hifiasm-HiC), state whether hap1 or hap2 (or mat/pat) is to be curated. The field contents should be either "hap1" or "hap2". This field is required for assemblies where the hap1 or hap2 fields are used. If both hap1 and hap2 are to be curated, submit two curation request emails with distinct YAML for the two cases. Note that the ticket can list locations for both hap1 and hap2 using the "hap1" and "hap2" fields, but only one can be specified for curation in a given YAML file via the haplotype_to_curate field.
transferred_chromosome: For assemblies where a sex chromosome has been transferred from the alternate parental haplotype to create a hybrid assembly with representation of all chromosomes, list that chromosome, eg "X", W".
combine_for_curation: For cases where a single HiC map is to be used for curation for multiple input FASTAs. This should be set to "true" for cases where haps and primary are to be combined. This will result in renaming of primary and haplotigs scaffolds to "HAP1_SCAFFOLD_N" and "HAP2_SCAFFOLD_N" and will mean that only the "additional haplotigs" file in the curation directory is used for submissions, with the pre-curation haplotig file not being used. Default is False.
haplotig_chromosomes: Set to "True" if haplotigs are assembled into chromosomes, and a chromosome list file has been provided for them. Default is False.
chromosome_list: Chromosome list file in TSV format for ENA. This will not normally be needed as the chromosome list file will be created during curation, but for cases where curation is to be omitted, the chromosome list file can created prior to the curation request and supplied here.
mito_gb: Genbank format file for MT
primary_contigs: Gzipped FASTA file for contig-level primary assembly
jira_queue: JIRA queue: GRIT, RC, or DS. This is normally not necessary, and should only be filled in to override default behaviour. DS is the Draft Submission queue, used for cases that do not require curation and which will just be contamination-checked and submitted
assembly_type: Assembly type to be supplied in ENA manifest file for assemblies that are to be directly submitted to ENA. By default this is assumed to be 'clone or isolate'. Other options: primary metagenome, binned metagenome, Metagenome-Assembled Genome (MAG)
priority: Priority for ticket. This is "Medium" by default. Permitted options are: Highest, High, Medium, Low, Lowest
hold_submission: True or False; this is False by default. If True, submission will be delayed until manual intervention occurs
agp: AGP file, if it exists (not needed in most cases)
hic: Hi-C data file in .hic format (note that the hic reads are listed separately)
[MANDATORY_FOR_NON_SANGER] pacbio_read_dir: Directory containing PacBio read files, preferably as fasta.gz. This should be the directory directly containing the read files rather than a parent directory.
[MANDATORY_FOR_NON_SANGER] ont_read_dir: Directory containing ONT read files, preferably as fasta.gz
[MANDATORY_FOR_NON_SANGER] 10x_read_dir: Directory containing 10X read files, preferably as fastq.gz
[MANDATORY_FOR_NON_SANGER] hic_read_dir: Directory containing HiC read files, preferably as unaligned reads as CRAM
[MANDATORY_FOR_NON_SANGER] bionano_cmap_dir: Directory containing BioNano data, preferably as CMAP
[MANDATORY_FOR_NON_SANGER] assembled_by_group: Group or institution that carried out the assembly. For assemblies produced within the Sanger Institute, this should be the Programme: ToL, Parasites, etc. For assemblies produced outside the Sanger, this should be the institution: UCSC, Rockefeller, Earlham. This is not required for standard Darwin/ASG assemblies produced at the Sanger, as ticket processing scripts will assume the institution is "ToL" (Tree of Life). The main purpose of this field is to distinguish between ToL and non-ToL assemblies, so if the institution naming or affiliation is complex for any reason, just put something in that makes it clear it's not a ToL assembly.
pacbio_read_type: Read type, eg "hifi"
pacbio_read_files: YAML list of PacBio read file locations. This should only be used for data sources like the ERGA NextCloud where it's not practical to obtain a directory listing and therefore the pacbio_read_dir argument given above can't be used to automatically download read files. Provide the complete path to each file.
hic_kit: HiC kit type, distinguishing between Arima and Arima2
ploidy: Ploidy of assembly. This is assumed to be "diploid" by default, so this does not need to be specified for diploid assemblies. Other ploidies should be specified: haploid, triploid, tetraploid, hexaploid, etc.
karyotype: Karyotype if known. This will be retrieved from GOAT for Darwin assemblies if present
karyotype_source: Source of karyotype data
pretext: Pretext file
hap1_pretext: Pretext file for the first haplotype-resolved assembly (eg from Hifiasm-HiC)
hap2_pretext: Pretext file for the second haplotype-resolved assembly (eg from Hifiasm-HiC)
hic_map_img: Hi-C map image file
linear_mito: Is the mitochondrial sequence linear- true or false? (False by default) This is deprecated in favour of the below.
organelle_topology: Optional YAML dictionary of organelle scaffold names and whether they are linear or circular. See example below.
kmer_spectra_img: Kmer spectrum image file
busco_lineage: Lineage parameter used for running BUSCO
busco_gene_set_species: Gene set species used for running BUSCO
biosample: BioSample for submission. Usually only needed for Sanger faculty assemblies.
run_accessions: Comma-separated list of related run accessions.  Should *only* contain run accessions, eg 'ERS18389291'. Usually only needed for Sanger faculty assemblies.
coverage: Number for coverage, eg "45". No need for an "x" on the end, only the number is needed. Usually only needed for Sanger faculty assemblies.
assembly_description: Descriptive text for INSDC Assembly database submission. Usually only needed for Sanger faculty assemblies.
bioproject_abstract: Descriptive text for INSDC BioProject database submission. Usually only needed for Sanger faculty assemblies.
taxonomy_id: NCBI taxonomy ID. This is *not* normally required, as the scripts derive the taxonomy ID from the species name, but this can be useful for cases where the preferred species name is not in the NCBI taxonomy database.
pipeline: YAML list of software used, in order, to create the assembly. Version numbers for software should follow in parentheses, eg "hifiasm (0.16.1-r375)"
notes: Notes on the assembly. Free text field. For assemblies that should be downloaded by password-protected FTP, if you're happy to share the login details via email and store them in our JIRA system, include them in this field like this: "notes: Username=USERNAMEHERE Password=12345"
stats: Statistics for the assembly. Free text field
```

Example:
```
---
species: Sacculina carcini
specimen: qxSacCarc1
projects:
  - darwin
release_to: ENA
primary: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/qxSacCarc1.PB.asm2.purge3.polish1.scaff1.fa.gz
haplotigs: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/qxSacCarc1.PB.asm2.purge3.polish1.scaff1.haplotigs.fa.gz
mito: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/qxSacCarc1-MT.fa.gz
mito_gb: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/qxSacCarc1-MT.gb
primary_contigs: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/qxSacCarc1.PB.asm2.purge3.polish1.primary_contigs.fa.gz
agp: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/scaffolds_FINAL.agp
hic: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/salsa_scaffolds.hic
pacbio_read_dir: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/genomic_data/qxSacCarc1/pacbio/fasta/
ont_read_dir: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/genomic_data/qxSacCarc1/ont/fasta/
10x_read_dir: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/genomic_data/qxSacCarc1/10x/
hic_read_dir: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/genomic_data/qxSacCarc1/hic-arima2/
pacbio_read_type: hifi
hic_kit: Arima2
karyotype: None
karyotype_source: 
pretext: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/scaffolds_FINAL.pretext
hic_map_img: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/scaffolds_FINAL_FullMap.png
kmer_spectra_img: /lustre/scratch116/tol/projects/darwin/data/arthropods/Sacculina_carcini/assembly/draft/qxSacCarc1.PB.asm2.purge3.polish1.scaff1/meryl.scaffolds_FINAL.spectra-cn.fl.png
pipeline:
  - hifiasm (0.14-r312)
  - purge_dups (version 1.2.3) 
  - longranger (version 2.2.2)
  - freebayes (v1.3.1-17-gaa2ace8)
  - MitoHiFi(v2.11.3) # no mito found
  - salsa (v2.2)
organelle_topology:
  SCAFFOLD_1: Linear
  SCAFFOLD_2: Circular
  SCAFFOLD_3: Linear
notes: 
  Haven't being able to find a mito after several attempts
stats: |
  merqury QV 45
  C:99.6%[S:99.0%,D:0.6%],F:0.1%,M:0.3%,n:1013 
  
  qxSacCarc1.PB.asm2.purge3.polish1.scaff1.fa.gz
  COMPOSITION     A = 81000915 (29.1%), C = 58137282 (20.9%), G = 58249900 (20.9%), T = 80615500 (29.0%), N = 45000 (0.0%), CpG = 21814428 (7.8%)
  SCAFFOLD        sum = 278048597, n = 231, mean = 1203673.58008658, largest = 14783571, smallest = 5135
  SCAFFOLD        N50 = 7273666, L50 = 15
  SCAFFOLD        N60 = 6372685, L60 = 19
  SCAFFOLD        N70 = 5721615, L70 = 23
  SCAFFOLD        N80 = 4626355, L80 = 29
  SCAFFOLD        N90 = 3877963, L90 = 35
  SCAFFOLD        N100 = 5135, L100 = 231
  CONTIG  sum = 278003597, n = 321, mean = 866054.819314642, largest = 10238334, smallest = 5135
  CONTIG  N50 = 3477386, L50 = 28
  CONTIG  N60 = 2910235, L60 = 36
  CONTIG  N70 = 2461089, L70 = 46
  CONTIG  N80 = 1520263, L80 = 61
  CONTIG  N90 = 880610, L90 = 85
  CONTIG  N100 = 5135, L100 = 321
  GAP     sum = 45000, n = 90, mean = 500, largest = 500, smallest = 500
```

Here is a typical format for a GenomeArk VGP assembly:
```
---
species: Gopherus flavomarginatus
specimen: rGopFla2
projects:
 - VGP orders
release_to: S3
data_location: S3
mito: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/assembly_MT_rockefeller/rGopFla2.MT.20191216.fasta.gz
maternal: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/assembly_nhgri_trio_1.6/rGopFla2.mat.asm.20210721.fasta.gz
paternal: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/assembly_nhgri_trio_1.6/rGopFla2.pat.asm.20210721.fasta.gz
pacbio_read_dir: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/genomic_data/pacbio/
10x_read_dir: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/genomic_data/10x/
hic_read_dir: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/genomic_data/arima/
bionano_cmap_dir: s3://genomeark/species/Gopherus_flavomarginatus/rGopFla2/genomic_data/bionano/
assembled_by_group: Rockefeller
```

Please contact James Torrance (jt8@sanger.ac.uk) if you have any questions about the format.
